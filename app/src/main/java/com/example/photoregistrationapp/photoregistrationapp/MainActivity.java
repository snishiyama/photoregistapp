package com.example.photoregistrationapp.photoregistrationapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private LinearLayout mainView;
    private EditText editText;
    private Spinner editspinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainView = (LinearLayout) findViewById(R.id.container);
        editText = (EditText) findViewById(R.id.editText);
        editspinner = (Spinner) findViewById(R.id.spinner);

        Button registActivity = findViewById(R.id.button7);
        registActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), RegisterActivity.class);
                startActivity(intent);
            }
        });

    }

    public void onAddNexItemonClicked(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("写真を登録します。よろしいですか？")
                .setNegativeButton("キャンセル", null)
                .setPositiveButton("登録", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // 新規登録画面のアイテムの値を取得
                        int idx = editspinner.getSelectedItemPosition();
                        String categoryValue = (String)editspinner.getSelectedItem();
                        String commentValue = editText.getText().toString();

                        //activity_addnewitem.xmlのアイテムに値をセット
                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View itemView = inflater.inflate(R.layout.activity_addnewitem, null);
                        final TextView category = (TextView) itemView.findViewById(R.id.category);
                        final TextView comment = (TextView) itemView.findViewById(R.id.comment);
                        category.setText(categoryValue);
                        comment.setText(commentValue);

                        // 「登録ボタン」の下にレイアウトを挿入
                        mainView.addView(itemView,mainView.getChildCount());
                    }
                });
        builder.show();
    }

    public void onAddDeleteonClicked(View view) {
        /*
        // ボタンの親Viewを取得
        View rowView = (View) view.getParent();
        // その親 : 項目レイアウト[addnew_view]を取得
        LinearLayout rowContainer = (LinearLayout) rowView.getParent();
        mainView.removeView(rowContainer);
        */
    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.custom_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_delete:
                Intent intent = new Intent(this, SettingMenuFragment.class);
                startActivity(intent);
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

}
