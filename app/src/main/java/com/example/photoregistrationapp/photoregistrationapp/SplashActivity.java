package com.example.photoregistrationapp.photoregistrationapp;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.content.Intent;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //タイトルを非表示にする
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //splash.xmlをViewに指定する
        setContentView(R.layout.splash);
        Handler hdl = new Handler();
        //500ms遅延させてsplashHandler実行
        hdl.postDelayed(new splashHandler() ,1000);
    }

    class splashHandler implements Runnable{
        public void run(){
            //splash完了後に実行するActivityを指定する
            Intent intent = new Intent(getApplication() , MainActivity.class);
            startActivity(intent);
            //SplashActivityを終了させる
            SplashActivity.this.finish();
        }
    }
}
