package com.example.photoregistrationapp.photoregistrationapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

public class SettingMenuFragment extends AppCompatActivity {

    private EditText editText;
    CustomOpenHelper helper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_setting_menu);

        // edittextを取得
        editText = findViewById(R.id.editText);
        // 追加ボタン
        Button addButton = findViewById(R.id.button3);
        // 削除ボタン
        final Button deleteButton = findViewById(R.id.button4);

        // DBから取得したアイテムリストを保存するためのリストを定義
        ArrayList<String>spinnerList = new ArrayList<String>();

        // 削除に表示するスピナーを取得
        final Spinner deleteSpinner = (Spinner)findViewById(R.id._dynamic);

        // 設定反映に表示するスピナーを取得
        Spinner settingSpinner = (Spinner)findViewById(R.id._dynamic2);

        ArrayAdapter<String>adapter = new ArrayAdapter<String>(this, R.layout.fragment_setting_menu);

        if(helper == null){
            helper = new CustomOpenHelper(SettingMenuFragment.this);
        }
        // データベースに接続する
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            // rawQueryというSELECT専用メソッドを使用してデータを取得する
            Cursor c = db.rawQuery("select id, name from CATEGORY_TABLE", null);
            // Cursorの先頭行があるかどうか確認
            boolean next = c.moveToFirst();

            // 最終的に表示する文字列
            String dispStr = "";
            // 取得した全ての行を取得
            while (next) {
                // 取得したカラムの順番(0から始まる)と型を指定してデータを取得する
                String rowdata = String.valueOf(c.getInt(0))+" , ";// idを取得
                spinnerList.add( c.getString(1));// nameを取得

                // 次の行が存在するか確認
                next = c.moveToNext();
            }
            dispStr+= "取得完了";
            Log.d("test111", "onCreate:"+ dispStr + "");
        } finally {
            // finallyは、tryの中で例外が発生した時でも必ず実行される
            // dbを開いたら確実にclose
            db.close();
        }

        // スピナーリストのアダプターを定義し、DBから取得してきた値をセットする
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,spinnerList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deleteSpinner.setAdapter(arrayAdapter);

        // リスナーを登録
        deleteSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            //　アイテムが選択された時
            @Override
            public void onItemSelected(AdapterView<?> parent,
                                       View view, int position, long id) {
                Spinner spinner = (Spinner)parent;
                String item = (String)spinner.getSelectedItem();
                Log.d("item", "onItemSelected:"+ item+" ");
            }

            //　アイテムが選択されなかった
            public void onNothingSelected(AdapterView<?> parent) {
                //
            }
        });

        // 設定反映がわ
        settingSpinner.setAdapter(arrayAdapter);
        // リスナーを登録
        settingSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            //　アイテムが選択された時
            @Override
            public void onItemSelected(AdapterView<?> parent,
                                       View view, int position, long id) {
                Spinner spinner = (Spinner)parent;
                String settinngItem = (String)spinner.getSelectedItem();
                Log.d("item", "onItemSelected:"+ settinngItem+" ");
            }

            //　アイテムが選択されなかった
            public void onNothingSelected(AdapterView<?> parent) {
                //
            }
        });

        // 追加ボタンタップ
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // edittextの値を取得する
                String addText = editText.getText().toString();
                //アプリ内データには配列が保存出来ず一旦断念。。。
//                SharedPreferences prefs = getSharedPreferences("categoryData", Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = prefs.edit();
//                editor.putString("category1", addText);
//                editor.apply();
                Log.d("test111", "onClick:"+ addText+" ");

                // DBにInsertするメソッドを呼び出す(その際にEditTextの中身を渡す)
                updateCategoryTable(addText);
            }
        });

        // 削除ボタンタップ
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 現在Spinnerで選択されている文字列を取得する
                String selectDeleteCategory = (String)deleteSpinner.getSelectedItem();

                // 先ほど取得した文字列を渡して、削除SQLを叩く
                deleteCategoryTable(selectDeleteCategory);
            }
        });
    }


    // Categoryの行を更新
    private void updateCategoryTable(String category){
        // CustomOpenHelperクラスがまだインスタンス化されていなかったらインスタンス化する
        if(helper == null){
            helper = new CustomOpenHelper(SettingMenuFragment.this);
        }
        // データベースを取得する
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            // 変数利用のために?を代替し、あとでexecuteしてあげる
            db.execSQL("insert into CATEGORY_TABLE values(?, ?)",
                    new String[]{ null, category});

        } finally {
            // finallyは、tryの中で例外が発生した時でも必ず実行される
            // dbを開いたら確実にclose
            db.close();
        }
    }

    private void deleteCategoryTable(String deleteCategory){
        // CustomOpenHelperクラスがまだインスタンス化されていなかったらインスタンス化する
        if(helper == null){
            helper = new CustomOpenHelper(SettingMenuFragment.this);
        }
        // データベースを取得する
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            // Spinnerで選択されている値を削除する
            db.execSQL("delete from CATEGORY_TABLE where name = ?",
                    new String[]{deleteCategory});

        } finally {
            // finallyは、tryの中で例外が発生した時でも必ず実行される
            // dbを開いたら確実にclose
            db.close();
        }
    }

}